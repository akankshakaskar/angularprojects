import { Component, OnInit } from '@angular/core';
import { AuthorService } from './author.service';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css'],
  providers: [AuthorService]
})
export class AuthorComponent implements OnInit {

  title ="Author's List";
  authorList;
  constructor(service :AuthorService) { 
    this.authorList=service.getAuthorList();
  }

  ngOnInit() {
  }

}
