import { Component, OnInit } from '@angular/core';
import { CoursesService } from './courses.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css'],
  providers: [CoursesService]
})
export class CoursesComponent implements OnInit {

  title = " List of  Courses ";
  courses;

 // courses =["course1", "course2", "course3"];

  constructor(service: CoursesService) { 
    this.courses=service.getCourses();
  }

  ngOnInit() {
  }

 
  

}
